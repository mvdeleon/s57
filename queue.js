let collection = [];
// Write the queue functions below. 


//Gives all the elements of the queue
function print(){
    return(collection);

}

//Adds an element/elements to the end of the queue
function enqueue(item){
    collection.push(item); 
    return collection;

}

//Removes an element in front of the queue
function dequeue(){
    collection.shift();
    return collection;
}


//Shows the element at the front
function front(){
    return collection[0];
}

//Shows the total number of elements
function size(){
    return collection.length;
}

//Gives a Boolean value describing whether the queue is empty or not
function isEmpty(){
    return collection.length === 0;
}








// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};